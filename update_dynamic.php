#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;

$sparql2ids = array() ;

$db = openToolDB ( 'listeria_p' ) ;
$sql = "SELECT sparql,group_concat(id) AS ids FROM list GROUP BY sparql" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
while($o = $result->fetch_object()) {
	$sparql2ids[$o->sparql] = $o->ids ;
}

foreach ( $sparql2ids AS $sparql => $ids ) {
	if ( trim($sparql) == '' ) continue ;
/*	$url = "$wdq_internal_url?q=" . urlencode($wdq) ;
	$j = @file_get_contents ( $url ) ;
*/
	$varname = 'item' ; // Default
	if ( preg_match ( '/\?([a-zA-Z0-9_-]+)/' , str_replace("\n",' ',$sparql) , $m ) ) $varname = $m[1] ;
	$items = getSPARQLitems ( $sparql , $varname ) ;

	if ( count($items) == 0 ) {
		print "Error while running $sparql\n" ;
		continue ;
	}
	$noi = count ( $items ) ;
	$items = ',' . implode ( ',' , $items ) . ',' ;

	if ( !$db->ping() ) $db = openToolDB ( 'listeria_p' ) ; // Paranoia

	$sql = "UPDATE list SET last_update='" . date('c') . "',items='$items',number_of_items=$noi WHERE id IN ($ids)" ;

	if(!$result = $db->query($sql)) {
		print 'There was an error running the query [' . $db->error . ']'."\n\n$sql\n" ;
		continue ;
	}
}

?>